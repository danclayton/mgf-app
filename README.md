CRUD Application

A system allowing users to create, read, update, and delete Designs and Design issues for construction project installations.

Built from scratch.

App uses a PHP back-end, with HTML, CSS, and JavaScript front-end.
