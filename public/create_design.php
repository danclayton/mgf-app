<?php

//Add a new Design to the database


//Only run if form submitted

if (isset($_POST['submit'])) {

	//Database config

	require "config.php";
  require "common.php";


	try {
		$connection = new PDO($dsn, $username, $password, $options);

		// submitted values array

		$designs = array(

			"customer_id" 			=> $_POST['customer_id'],
			"location_id" 			=> $_POST['location_id'],
			"contractor_id" 		=> $_POST['contractor_id'],
			"title" 						=> $_POST['title'],
			"special_project"	 	=> $_POST['special_project'],
			"permanent_works" 	=> $_POST['permanent_works'],
			"depth" 						=> $_POST['depth'],
			"length" 						=> $_POST['length'],
			"width" 						=> $_POST['width'],
			"design_type_id" 		=> $_POST['design_type_id'],


		);

		//Prevents updating values in three places

		$sql = sprintf(
				"INSERT INTO %s (%s) values (%s)",
				"designs",
				implode(", ", array_keys($designs)),
				":" . implode(", :", array_keys($designs))
		);

		$statement = $connection->prepare($sql);
		$statement->execute($designs);

	} catch(PDOException $error) {
		echo $sql . "<br>" . $error->getMessage();
	}

}
?>

<!-- Pulls in header template-->


<?php include "templates/header.php"; ?>

<!-- Displays notification if POST submission was successful-->

<?php if (isset($_POST['submit']) && $statement) { ?>
	<blockquote><?php echo escape($_POST['customer_id']); ?> has been added to the database</blockquote>
<?php } ?>

<h2>Create a design</h2>

<!-- Create Design form -->

<form method="post" class="createForm" onsubmit="submissionFunction();">

	<!-- Create Design form -->

	<label for="customer_id">Customer ID</label>
	<input type="text" name="customer_id" id="customer_id" required>

		<label for="location_id">Location ID</label>
		<input type="text" name="location_id" id="location_id" required>

				<label for="contractor_id">Contrctor ID</label>
				<input type="text" name="contractor_id" id="contractor_id" required>

						<label for="title">Title</label>
  					<input type="text" name="title" id="title" required>

							<label for="special_project">Special Project</label>
							<input type="number" name="special_project" id="special_project" min="0" max="255" required>

								<label for="permanent_works">Permanent Works</label>
  							<input type="number" name="permanent_works" id="permanent_works" min="0" max="255" required>

								<label for="depth">Depth</label>
  							<input type="text" name="depth" id="depth" placeholder="Requires decimal" required >

							<label for="length">Length</label>
  						<input type="text" name="length" id="length" placeholder="Requires decimal" required>

						<label for="width">Width</label>
  					<input type="text" name="width" id="width" placeholder="Requires decimal" required>

				<label for="design_type_id">Design Type ID</label>
  			<input type="text" name="design_type_id" id="design_type_id" required>

			<br><br>

	<input type="submit" name="submit" value="Submit">

</form>


<!-- Pulls in Footer template -->


<?php include "templates/footer.php"; ?>
