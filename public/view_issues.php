<?php

//View table of Design Issues with edit/delete buttons

//Database config

require "config.php";
require "common.php";

//Database connection/settngs for design issue delete

if (isset($_GET["id"])) {
  try {
    $connection = new PDO($dsn, $username, $password, $options);

    $id = $_GET["id"];

    $sql = "DELETE FROM design_issues WHERE id = :id";

    $statement = $connection->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $success = "Issue successfully deleted";
  } catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
}

//View list of all issues

try {
  $connection = new PDO($dsn, $username, $password, $options);

  $sql = "SELECT * FROM design_issues";

  $statement = $connection->prepare($sql);
  $statement->execute();

  $result = $statement->fetchAll();
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}
?>

<!-- Pulls in header template-->

<?php require "templates/header.php"; ?>

<h2>Delete issues</h2>

<!-- Search feature with JS function 'searchIssueFunction()' -->

<input type="text" id="searchIssueInput" onkeyup="searchIssueFunction()" placeholder="Search issue by Design ID...">

<?php if ($success) echo $success; ?>

<!-- Design issues results table -->

<table id="listIssue">

  <thead>

    <tr>

        <th>#</th>
        <th>Design ID</th>
        <th>Category ID</th>
        <th>Description</th>
        <th>Date in</th>
        <th>Date out</th>
        <th>Designer ID</th>
        <th>Checker ID</th>
        <th>Status ID</th>
        <th>Drawing Req</th>

    </tr>

  </thead>

<tbody>

  <?php foreach ($result as $row) : ?>

    <tr>
        <td><?php echo escape($row["id"]); ?></td>
        <td><?php echo escape($row["design_id"]); ?></td>
        <td><?php echo escape($row["category_id"]); ?></td>
        <td><?php echo escape($row["description"]); ?></td>
        <td><?php echo escape($row["date_in"]); ?></td>
        <td><?php echo escape($row["date_out"]); ?></td>
        <td><?php echo escape($row["desinger_id"]); ?></td>
        <td><?php echo escape($row["checker_id"]); ?></td>
        <td><?php echo escape($row["status_id"]); ?></td>
        <td><?php echo escape($row["drawing_req"]); ?></td>
        <td class="editContainer"><a href="update_issues.php?id=<?php echo escape($row["id"]); ?>" class="editButton">Edit</a></td>
        <td class="deleteContainer"> <a href="view_issues.php?id=<?php echo escape($row["id"]); ?>" class="deleteButton">Delete</a></td>
    </tr>

  <?php endforeach; ?>

  </tbody>

</table>

<!-- Pulls in Footer template -->

<?php require "templates/footer.php"; ?>
