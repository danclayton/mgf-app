-- Dumping database structure for exercise
DROP DATABASE IF EXISTS `exercise`;
CREATE DATABASE IF NOT EXISTS `exercise` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `exercise`;


-- Dumping structure for table test.designs
DROP TABLE IF EXISTS `designs`;
CREATE TABLE IF NOT EXISTS `designs` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`customer_id` varchar(50) DEFAULT NULL,
`location_id` varchar(50) DEFAULT NULL,
`contractor_id` varchar(50) DEFAULT NULL,
`title` varchar(100) DEFAULT NULL,
`special_project` tinyint(1) unsigned DEFAULT NULL,
`permanent_works` tinyint(1) unsigned DEFAULT NULL,
`depth` decimal(13,2) DEFAULT NULL,
`length` decimal(13,2) DEFAULT NULL,
`width` decimal(13,2) DEFAULT NULL,
`design_type_id` varchar(10) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table test.designs: 0 rows
/*!40000 ALTER TABLE `designs` DISABLE KEYS */;
/*!40000 ALTER TABLE `designs` ENABLE KEYS */;


-- Dumping structure for table exercise.design_categories
DROP TABLE IF EXISTS `design_categories`;
CREATE TABLE IF NOT EXISTS `design_categories` (
`id` int(11) NOT NULL,
`description` varchar(255) NOT NULL,
`turnaround_time` varchar(255) NOT NULL,
`hex_color` varchar(255) NOT NULL,
`rgb_color` varchar(255) NOT NULL,
`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`created_by_id` varchar(32) NOT NULL,
`updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
`updated_by_id` varchar(32) NOT NULL,
`deleted_by_id` varchar(10) DEFAULT NULL,
`deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table exercise.design_categories: 8 rows
/*!40000 ALTER TABLE `design_categories` DISABLE KEYS */;
REPLACE INTO `design_categories` (`id`, `description`, `turnaround_time`, `hex_color`, `rgb_color`, `created_at`, `created_by_id`, `updated_at`, `updated_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(0, '-', '', '#999999', '153,153,153', '2018-08-13 08:33:37', '419', '2018-07-03 11:26:45', '419', NULL, NULL);
REPLACE INTO `design_categories` (`id`, `description`, `turnaround_time`, `hex_color`, `rgb_color`, `created_at`, `created_by_id`, `updated_at`, `updated_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(1, 'Conceptual', 'N/A', '#9933CC', '153,51,204', '2018-08-13 08:33:43', '419', '2018-07-03 11:26:49', '419', '419', '2017-11-03 10:55:04');
REPLACE INTO `design_categories` (`id`, `description`, `turnaround_time`, `hex_color`, `rgb_color`, `created_at`, `created_by_id`, `updated_at`, `updated_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(2, '0', '1 Day', '#0070C0', '0,112,192', '2018-08-13 08:33:48', '419', '2018-07-03 11:26:53', '419', NULL, NULL);
REPLACE INTO `design_categories` (`id`, `description`, `turnaround_time`, `hex_color`, `rgb_color`, `created_at`, `created_by_id`, `updated_at`, `updated_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(3, '1', '1 Day', '#92D050', '146,208,80', '2018-08-13 08:33:53', '419', '2018-07-03 11:26:57', '419', NULL, NULL);
REPLACE INTO `design_categories` (`id`, `description`, `turnaround_time`, `hex_color`, `rgb_color`, `created_at`, `created_by_id`, `updated_at`, `updated_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(4, '2A', '2 Days', '#FFC000', '255,192,0', '2018-08-13 08:33:58', '419', '2018-07-03 11:27:00', '419', NULL, NULL);
REPLACE INTO `design_categories` (`id`, `description`, `turnaround_time`, `hex_color`, `rgb_color`, `created_at`, `created_by_id`, `updated_at`, `updated_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(5, '2B', '3 Days', '#FF7C09', '255,124,9', '2018-08-13 08:34:03', '419', '2018-07-03 11:27:04', '419', NULL, NULL);
REPLACE INTO `design_categories` (`id`, `description`, `turnaround_time`, `hex_color`, `rgb_color`, `created_at`, `created_by_id`, `updated_at`, `updated_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(6, '2C', '3/4 Days', '#FF0000', '255,0,0', '2018-08-13 08:34:10', '419', '2018-07-03 11:27:07', '419', NULL, NULL);
REPLACE INTO `design_categories` (`id`, `description`, `turnaround_time`, `hex_color`, `rgb_color`, `created_at`, `created_by_id`, `updated_at`, `updated_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(7, '3', '5 Days', '#993366', '153,51,102', '2018-08-13 08:34:15', '419', '2018-07-03 11:27:11', '419', NULL, NULL);
/*!40000 ALTER TABLE `design_categories` ENABLE KEYS */;


-- Dumping structure for table exercise.design_engineers
DROP TABLE IF EXISTS `design_engineers`;
CREATE TABLE IF NOT EXISTS `design_engineers` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`userid` varchar(50) DEFAULT NULL,
`name` varchar(50) DEFAULT NULL,
`initial` varchar(5) DEFAULT NULL,
`grade` varchar(50) DEFAULT NULL,
`title` varchar(50) DEFAULT NULL,
`qualifications` varchar(50) DEFAULT NULL,
`eductaion` text,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table exercise.design_engineers: 2 rows
/*!40000 ALTER TABLE `design_engineers` DISABLE KEYS */;
REPLACE INTO `design_engineers` (`id`, `userid`, `name`, `initial`, `grade`, `title`, `qualifications`, `eductaion`) VALUES
(1, 'brian', 'Brian Griffin', '', 'a', 'Mr', 'mEng(2:ii)', NULL);
REPLACE INTO `design_engineers` (`id`, `userid`, `name`, `initial`, `grade`, `title`, `qualifications`, `eductaion`) VALUES
(2, 'jim', 'Jim Bowen', NULL, 'b', 'Mr', 'bTec', NULL);
/*!40000 ALTER TABLE `design_engineers` ENABLE KEYS */;


-- Dumping structure for table exercise.design_issues
DROP TABLE IF EXISTS `design_issues`;
CREATE TABLE IF NOT EXISTS `design_issues` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`design_id` int(10) unsigned DEFAULT NULL,
`category_id` int(3) unsigned DEFAULT NULL,
`description` varchar(50) DEFAULT NULL,
`date_in` datetime DEFAULT NULL,
`date_out` datetime DEFAULT NULL,
`designer_id` int(10) DEFAULT NULL,
`checker_id` int(10) DEFAULT NULL,
`status_id` int(10) unsigned DEFAULT NULL,
`drawing_req` tinyint(1) unsigned DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table exercise.design_issues: 0 rows
/*!40000 ALTER TABLE `design_issues` DISABLE KEYS */;
/*!40000 ALTER TABLE `design_issues` ENABLE KEYS */;


-- Dumping structure for table exercise.design_senior_engineers
DROP TABLE IF EXISTS `design_senior_engineers`;
CREATE TABLE IF NOT EXISTS `design_senior_engineers` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`userid` varchar(50) DEFAULT NULL,
`name` varchar(50) DEFAULT NULL,
`initial` varchar(5) DEFAULT NULL,
`grade` varchar(50) DEFAULT NULL,
`title` varchar(50) DEFAULT NULL,
`qualifications` varchar(50) DEFAULT NULL,
`eductaion` text,
`synopsis` text,
`memberships` text,
`experience` text,
PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10003 DEFAULT CHARSET=utf8;

-- Dumping data for table exercise.design_senior_engineers: 2 rows
/*!40000 ALTER TABLE `design_senior_engineers` DISABLE KEYS */;
REPLACE INTO `design_senior_engineers` (`id`, `userid`, `name`, `initial`, `grade`, `title`, `qualifications`, `eductaion`, `synopsis`, `memberships`, `experience`) VALUES
(10001, 'jane', 'Jane McDonald', 'S', 'r', 'Mrs', NULL, '1st Class BSc Civil Engineering', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in magna purus. Donec interdum quis est in ultrices. Integer nec ex vitae neque consequat aliquam sed nec neque. In sem purus, egestas at interdum convallis, egestas ut erat. Morbi posuere justo id erat porta consequat. Praesent purus est, eleifend sit amet libero ut, congue venenatis turpis. Curabitur tristique, lorem eget ornare dignissim, lorem sapien bibendum enim, eget fermentum quam ex vel metus. Nam condimentum tellus nec nibh egestas, eu finibus nisl gravida.', 'Chartered Member Institute of Civil Engineers', 'Vivamus efficitur urna nulla, porttitor venenatis leo scelerisque vitae. Cras non turpis magna. Vestibulum laoreet neque lobortis ligula tristique pulvinar. Ut molestie gravida rutrum. Integer sagittis mi mauris, vel interdum ex suscipit vel. Mauris eleifend scelerisque mauris mattis mollis. Mauris urna ante, fringilla eu convallis eget, congue vel tortor. Morbi varius dapibus purus, at imperdiet orci viverra nec. Nulla sit amet vulputate augue. Sed efficitur tincidunt dolor, eu porttitor eros dapibus a. Sed dignissim condimentum ipsum pellentesque maximus. Fusce porta placerat pellentesque. Proin finibus a ipsum vel accumsan. Nulla et eleifend metus, porta tempus magna.');
REPLACE INTO `design_senior_engineers` (`id`, `userid`, `name`, `initial`, `grade`, `title`, `qualifications`, `eductaion`, `synopsis`, `memberships`, `experience`) VALUES
(10002, 'bart', 'Bart Simpson', 'H', 's', 'Mr', 'BSc (Hons)', NULL, 'Cras dignissim ipsum suscipit ipsum sagittis, ut luctus justo aliquet. Nam sollicitudin justo cursus pulvinar imperdiet. Aenean in urna dapibus, tincidunt nulla non, volutpat nisl. Etiam posuere eleifend ipsum sed tempor. Proin quis tincidunt massa. Aenean quis volutpat nisl. Quisque eget turpis ut justo aliquet consectetur id nec ante. Phasellus laoreet porttitor orci, eu molestie velit. Sed ornare imperdiet iaculis. Aliquam erat volutpat. Mauris eget urna rhoncus, auctor odio at, ultricies mi. Aenean et massa lobortis, sagittis tellus vitae, dapibus ante. Nunc ac felis nec massa sodales vestibulum. Maecenas luctus fringilla facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut libero mi.', 'Chartered Member of Institution of Structural Engineers', NULL);
/*!40000 ALTER TABLE `design_senior_engineers` ENABLE KEYS */;


-- Dumping structure for table exercise.design_statuses
DROP TABLE IF EXISTS `design_statuses`;
CREATE TABLE IF NOT EXISTS `design_statuses` (
`id` int(11) NOT NULL,
`description` varchar(255) NOT NULL,
`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`created_by_id` varchar(32) NOT NULL,
`modified_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
`modified_by_id` varchar(32) NOT NULL,
`deleted_by_id` varchar(10) DEFAULT NULL,
`deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table exercise.design_statuses: 10 rows
/*!40000 ALTER TABLE `design_statuses` DISABLE KEYS */;
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(0, 'Awaiting Rep Approval', '2018-07-27 10:53:26', '419', '2018-07-27 10:53:46', '419', NULL, NULL);
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(1, 'Awaiting Review', '2016-10-25 15:12:44', '419', '2018-08-13 08:08:50', '419', NULL, NULL);
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(2, 'Reviewed', '2018-08-13 08:31:51', '419', '2018-08-13 08:09:01', '419', NULL, NULL);
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(3, 'On Hold', '2018-08-13 08:31:58', '419', '2018-08-13 08:09:05', '419', NULL, NULL);
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(4, 'Design', '2018-08-13 08:32:02', '419', '2018-08-13 08:09:08', '419', NULL, NULL);
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(5, 'Awaiting Check', '2018-08-13 08:32:06', '419', '2018-08-13 08:09:15', '419', NULL, NULL);
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(6, 'Check', '2018-08-13 08:32:09', '419', '2018-08-13 08:09:18', '419', NULL, NULL);
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(7, 'Approved', '2018-08-13 08:32:13', '419', '2018-08-13 08:09:21', '419', NULL, NULL);
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(8, 'Complete', '2018-08-13 08:32:16', '419', '2018-08-13 08:09:25', '419', NULL, NULL);
REPLACE INTO `design_statuses` (`id`, `description`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
(9, 'Dead End', '2018-08-13 08:32:19', '419', '2018-08-13 08:09:30', '419', NULL, NULL);
/*!40000 ALTER TABLE `design_statuses` ENABLE KEYS */;


-- Dumping structure for table exercise.design_types
DROP TABLE IF EXISTS `design_types`;
CREATE TABLE IF NOT EXISTS `design_types` (
`id` varchar(10) NOT NULL,
`description` varchar(100) NOT NULL,
`icon` varchar(255) NOT NULL,
`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`created_by_id` varchar(32) NOT NULL,
`modified_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
`modified_by_id` varchar(32) NOT NULL,
`deleted_by_id` varchar(10) DEFAULT NULL,
`deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table exercise.design_types: 6 rows
/*!40000 ALTER TABLE `design_types` DISABLE KEYS */;
REPLACE INTO `design_types` (`id`, `description`, `icon`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
('ess', 'Excavation Support', 'mgf-icon-design-type-excavation-support', '2018-08-13 08:33:14', '419', '2017-01-20 09:29:21', '419', NULL, NULL);
REPLACE INTO `design_types` (`id`, `description`, `icon`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
('f', 'Frame Only', 'mgf-icon-frame-only', '2017-11-03 10:45:33', '419', '2017-11-07 09:22:08', '419', NULL, NULL);
REPLACE INTO `design_types` (`id`, `description`, `icon`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
('mp', 'Basement', 'mgf-icon-design-type-basement', '2018-08-13 08:32:56', '419', '2017-01-20 09:29:21', '419', NULL, NULL);
REPLACE INTO `design_types` (`id`, `description`, `icon`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
('o', 'Other', 'uk-icon-question', '2018-08-13 08:33:00', '419', '2017-01-20 09:29:21', '419', NULL, NULL);
REPLACE INTO `design_types` (`id`, `description`, `icon`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
('pw', 'Permanent Sheet Pile', 'mgf-icon-design-type-permanent', '2018-08-13 08:33:04', '419', '2017-01-20 09:29:21', '419', NULL, NULL);
REPLACE INTO `design_types` (`id`, `description`, `icon`, `created_at`, `created_by_id`, `modified_timestamp`, `modified_by_id`, `deleted_by_id`, `deleted_at`) VALUES
('sss', 'Structural Support', 'mgf-icon-design-type-structural-support', '2018-08-13 08:33:09', '419', '2017-01-20 09:29:21', '419', NULL, NULL);
/*!40000 ALTER TABLE `design_types` ENABLE KEYS */;



