<?php
//database config

$host       = "localhost";
$username   = "root";
$password   = "root";
$dbname     = "exercise";
$dsn        = "mysql:host=$host;dbname=$dbname";
$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );
