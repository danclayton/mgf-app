<?php

//Form to edit individual existing Design Issues


//Database config

require "config.php";
require "common.php";


//Only run if form submitted

if (isset($_POST['submit'])) {
  try {
    $connection = new PDO($dsn, $username, $password, $options);
    $xuser =[

      // submitted values array

      "id"            => $_POST['id'],
      "design_id"     => $_POST['design_id'],
      "category_id"   => $_POST['category_id'],
      "description"   => $_POST['description'],
      "date_in"       => $_POST['date_in'],
      "date_out"      => $_POST['date_out'],
      "designer_id"   => $_POST['designer_id'],
      "checker_id"    => $_POST['checker_id'],
      "status_id"     => $_POST['status_id'],
      "drawing_req"   => $_POST['drawing_req']

    ];

//Update database

    $sql = "UPDATE design_issues

            SET id = :id,

              design_id = :design_id,
              category_id = :category_id,
              description = :description,
              date_in = :date_in,
              date_out = :date_out,
              designer_id = :designer_id,
              checker_id = :checker_id,
              status_id = :status_id,
              drawing_req = :drawing_req

            WHERE id = :id";

  $statement = $connection->prepare($sql);
  $statement->execute($xuser);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
}

//Shows values for update form

if (isset($_GET['id'])) {
  try {
    $connection = new PDO($dsn, $username, $password, $options);
    $id = $_GET['id'];
    $sql = "SELECT * FROM design_issues WHERE id = :id";
    $statement = $connection->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $xuser = $statement->fetch(PDO::FETCH_ASSOC);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
} else {
    echo "Something went wrong!";
    exit;
}
?>

<!-- Pulls in header template-->

<?php require "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) : ?>
	<blockquote><?php echo escape($_POST['designer_id']); ?> successfully updated.</blockquote>
<?php endif; ?>

<h2>Edit an issue</h2>

<!-- Back button to quickly and easily return to display results -->

<button id="backButton" onclick="backFunction()">Back</button>

<!-- Update form -->

<form method="post" class="createForm">

    <?php foreach ($xuser as $key => $value) : ?>

      <label for="<?php echo $key; ?>"><?php echo ucfirst($key); ?></label>
	    <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo escape($value); ?>" <?php echo ($key === 'id' ? 'readonly' : null); ?>>

    <?php endforeach; ?>

    <br><br>

    <input type="submit" name="submit" value="Submit">

</form>


<!-- Pulls in Footer template -->


<?php require "templates/footer.php"; ?>
