<?php

//Form to edit individual existing Designs


//Database config

require "config.php";
require "common.php";


//Only run if form submitted

if (isset($_POST['submit'])) {
  try {
    $connection = new PDO($dsn, $username, $password, $options);
    $xuser =[

      // submitted values array

      "id"                => $_POST['id'],
      "customer_id"       => $_POST['customer_id'],
      "location_id"       => $_POST['location_id'],
      "contractor_id"     => $_POST['contractor_id'],
      "title"             => $_POST['title'],
      "special_project"   => $_POST['special_project'],
      "permanent_works"   => $_POST['permanent_works'],
      "depth"             => $_POST['depth'],
      "length"            => $_POST['length'],
      "width"             => $_POST['width'],
      "design_type_id"    => $_POST['design_type_id']

    ];


    //Update database

    $sql = "UPDATE designs

            SET id = :id,

              customer_id = :customer_id,
              location_id = :location_id,
              contractor_id = :contractor_id,
              title = :title,
              special_project = :special_project,
              permanent_works = :permanent_works,
              depth = :depth,
              length = :length,
              width = :width,
              design_type_id = :design_type_id

            WHERE id = :id";

  $statement = $connection->prepare($sql);
  $statement->execute($xuser);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
}

//Shows values for update form


if (isset($_GET['id'])) {
  try {
    $connection = new PDO($dsn, $username, $password, $options);
    $id = $_GET['id'];
    $sql = "SELECT * FROM designs WHERE id = :id";
    $statement = $connection->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $xuser = $statement->fetch(PDO::FETCH_ASSOC);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
} else {
    echo "Something went wrong!";
    exit;
}
?>

<!-- Pulls in header template-->


<?php require "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) : ?>
	<blockquote><?php echo escape($_POST['customer_id']); ?> successfully updated.</blockquote>
<?php endif; ?>

<h2>Edit a design</h2>

<!-- Back button to quickly and easily return to display results -->

<button id="backButtonDesigns" onclick="backButtonDesignsFunction()">Back</button>

<!-- Update form -->

<form method="post" class="createForm">

    <?php foreach ($xuser as $key => $value) : ?>

      <label for="<?php echo $key; ?>"><?php echo ucfirst($key); ?></label>
	    <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo escape($value); ?>" <?php echo ($key === 'id' ? 'readonly' : null); ?>>

    <?php endforeach; ?>

    <br><br>

    <input type="submit" name="submit" value="Submit">

</form>


<!-- Pulls in Footer template -->


<?php require "templates/footer.php"; ?>
