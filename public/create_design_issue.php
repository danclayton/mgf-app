<?php

//Add a new Design issue to the database


//Only run if form submitted


if (isset($_POST['submit'])) {

	//Database config

	require "config.php";
  require "common.php";


	try {
		$connection = new PDO($dsn, $username, $password, $options);

		// submitted values array


		$design_issues = array(

			"design_id" 			=> $_POST['design_id'],
			"category_id" 		=> $_POST['category_id'],
			"description" 		=> $_POST['description'],
			"date_in" 				=> $_POST['date_in'],
			"date_out" 				=> $_POST['date_out'],
			"designer_id" 		=> $_POST['designer_id'],
			"checker_id" 			=> $_POST['checker_id'],
			"status_id" 			=> $_POST['status_id'],
			"drawing_req" 		=> $_POST['drawing_req'],


		);

		//Prevents updating values in three places

		$sql = sprintf(
				"INSERT INTO %s (%s) values (%s)",
				"design_issues",
				implode(", ", array_keys($design_issues)),
				":" . implode(", :", array_keys($design_issues))
		);

		$statement = $connection->prepare($sql);
		$statement->execute($design_issues);

	} catch(PDOException $error) {
		echo $sql . "<br>" . $error->getMessage();
	}

}
?>

<!-- Pulls in header template-->


<?php include "templates/header.php"; ?>

<!-- Displays notification if POST submission was successful-->

<?php if (isset($_POST['submit']) && $statement) { ?>
		<blockquote id="entrySuccess"><?php echo escape($_POST['design_id']);
	?> has been added to the database</blockquote>
<?php } ?>

<h2>Input issue</h2>

<!-- Create Design Issue form -->

<form method="post" class="createForm" onsubmit="submissionFunction()">

	<label for="design_id">Design ID</label>
	<input type="text" name="design_id" id="design_id" required>

		<label for="category_id">Category ID</label>
		<input type="text" name="category_id" id="category_id" required>

			<label for="Description">Description</label>
			<input type="text" name="description" id="description" required>

					<label for="date_in">Date In</label>
  				<input type="text" name="date_in" id="date_in" placeholder="YYYY-MM-DD 00:00:00" required>

						<label for="date_out">Date Out</label>
						<input type="text" name="date_out" id="date_out" placeholder="YYYY-MM-DD 00:00:00" required>

							<label for="designer_id">Designer ID</label>
  						<input type="text" name="designer_id" id="designer_id" required>

						<label for="checker_id">Checker ID</label>
  					<input type="text" name="checker_id" id="checker_id" required>

					<label for="status_id">Status ID</label>
  				<input type="text" name="status_id" id="status_id" required>

			<label for="drawing_req">Drawing Req</label>
  	<input type="number" name="drawing_req" id="drawing_req" min="0" max="255" required>

	<br><br>

	<input type="submit" name="submit" value="Submit">
</form>



<?php include "templates/footer.php"; ?>
