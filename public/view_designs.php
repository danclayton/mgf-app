<?php

//View table of Designs with edit/delete buttons

//Database config

require "config.php";
require "common.php";

//Database connection/settngs for Designs delete

if (isset($_GET["id"])) {
  try {
    $connection = new PDO($dsn, $username, $password, $options);

    $id = $_GET["id"];

    $sql = "DELETE FROM designs WHERE id = :id";

    $statement = $connection->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $success = "Design successfully deleted";
  } catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
}

//View list of all Designs

try {
  $connection = new PDO($dsn, $username, $password, $options);

  $sql = "SELECT * FROM designs";

  $statement = $connection->prepare($sql);
  $statement->execute();

  $result = $statement->fetchAll();
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}
?>

<!-- Pulls in header template-->

<?php require "templates/header.php"; ?>

<h2>Designs</h2>

<!-- Search feature with JS function 'searchFunction()' -->

<input type="text" id="searchInput" onkeyup="searchFunction()" placeholder="Search by Customer ID...">


<?php if ($success) echo $success; ?>

<!-- Designs results table -->


<table id="list">

  <thead>

    <tr>

        <th>#</th>
        <th>Customer ID</th>
        <th>Location ID</th>
        <th>Contractor_id</th>
        <th>Title</th>
        <th>Special Project</th>
        <th>Permanent Works</th>
        <th>Depth</th>
        <th>Length</th>
        <th>Width</th>
        <th>Design Type ID</th>

    </tr>

  </thead>

<tbody>

    <?php foreach ($result as $row) : ?>

    <tr>
        <td><?php echo escape($row["id"]); ?></td>
        <td><?php echo escape($row["customer_id"]); ?></td>
        <td><?php echo escape($row["location_id"]); ?></td>
        <td><?php echo escape($row["contractor_id"]); ?></td>
        <td><?php echo escape($row["title"]); ?></td>
        <td><?php echo escape($row["special_project"]); ?></td>
        <td><?php echo escape($row["permanent_works"]); ?></td>
        <td><?php echo escape($row["depth"]); ?></td>
        <td><?php echo escape($row["length"]); ?></td>
        <td><?php echo escape($row["width"]); ?></td>
        <td><?php echo escape($row["design_type_id"]); ?></td>
        <td class="editContainer"><a href="update_designs.php?id=<?php echo escape($row["id"]); ?>" class="editButton">Edit</a></td>
        <td class="deleteContainer"><a href="view_designs.php?id=<?php echo escape($row["id"]); ?>" class="deleteButton">Delete</a></td>
    </tr>

  <?php endforeach; ?>

  </tbody>

</table>

<!-- Pulls in Footer template -->

<?php require "templates/footer.php"; ?>
