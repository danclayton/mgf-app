
// Alert box function displays after user submits create_design/create__design_issue.php forms

function submissionFunction() {

  alert("Your form has been submitted");

}


//--------------------------------------------------------------------


//Search feature to show results by Customer ID in view_designs.php

function searchFunction() {

  //Declaring variables

     var input, filter, table, tr, td, i;
  input = document.getElementById("searchInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("list");
  tr = table.getElementsByTagName("tr");

  //Looping through rows in results

  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}


//--------------------------------------------------------------------


//Search feature to show results by Customer ID in view_issues.php


function searchIssueFunction() {

  //Declaring variables

     var input, filter, table, tr, td, i;
  input = document.getElementById("searchIssueInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("listIssue");
  tr = table.getElementsByTagName("tr");

  //Looping through rows in results

  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}


//--------------------------------------------------------------------


//Back button located on update issue page to quickly and easily return to display results

function backFunction() {

  window.location.href="view_issues.php";

}

//Back button located on update Designs page to quickly and easily return to display results

function backButtonDesignsFunction() {

  window.location.href="view_designs.php";

}
